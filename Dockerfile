# Base image
FROM debian:jessie

# Node version
FROM node:11

# Create app directory
WORKDIR /usr/src/songKickAPI

# Install app dependencies
COPY package*.json ./

RUN npm install --only=production
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

# EXPOSE 1234
RUN npm run build
CMD ["npm", "start"]