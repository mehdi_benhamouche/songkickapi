# SongKickAPI

This is a PoC of designing a Nodejs based service using typescript.This Service is tested by using mocha framework and chai libraray for asserations. The database is a mongo one. Mongoose is used as ODM.

## Database Model
![alt text](./out/archi/database/entity/database_model_digram.png?at=master)

## Application architecture
Here, all modules (or components) are classes. See the digram below.

![alt text](./out/archi/app/app/app_class_digram.png?at=master)

## How to use?
### Clone
```git clone https://gitlab.com/mehdi_benhamouche/songkickapi```
### Using docker compose:
Firstly, build the docker images (api, databse),

```docker-compose buld``` or ```docker-compose up -d```

when the mongo-songkick container is up, import the data from the ``bands.json``, ``concerts.json`` and ``venues.json`` wich are already copied into the container using the mongo dockerfile,

```docker exec -it mongo-songkick-container /bin/bash insert.sh ```

The service is externaly exposed on port ``5001`` as it can be seen into the docker-compose.yml file.

### Otherwise:

#### - install:
```npm install```

#### - test:
```npm run test-details```

#### - build:
```npm run build```

#### - run:
```npm start```



## QRs
* <b> Describe in detail how you would store and query the data, and what kind of mechanism you would leverage to consistently deliver short response times and guarantee the highest uptime possible:</b>

Firstly, we have to store the de data into deferent databases using deticated SSDs (ref: [mongo-1]). We can keep the entity model discribed above. We have to implement detecated service for each database. These will be invoked by a main service as described below.

![alt text](./out/archi/scaling/app/scalable_archi.png?at=master)

We use virtuals  (ref: [mongo-3]) and cross database populating (ref: [mongo-2])

The services ``` bandService```, ``` concertService``` and ``` venueService``` in this part of code, 

```javascript
const bandQuery = {};
        const promises = [];
        if (bandIds) {
            assign(bandQuery, { id: { $in: bandIds } });
        }
        promises.push(this.bandService.getBands(bandQuery));
        if (radius && longitude && latitude) {
            promises.push(this.venueService.getVenues({}));
        } else {
            promises.push(Promise.resolve([]));
        }
        return BlueBirdPromise.all(promises)
            .spread((bands, venues) => {
                const concertQuery = { $and: [] };
                if (venues.length) {
                    // filter operation
                    const venueIds = filter(venues, (v: VenueInterface) => {
                        return DataBaseTool.isAround(v, latitude, longitude, radius);
                    }).map((v: VenueInterface) => {
                        return v.id;
                    });
                    concertQuery.$and.push({ venueId: { $in: venueIds } });
                }
                if (bands.length) {
                    const bandIds = map(bands, (b: BandInterface) => {
                        return b.id;
                    });
                    concertQuery.$and.push({ bandId: { $in: bandIds } });
                }
                if (concertQuery.$and === []) concertQuery.$and = undefined;
                return this.concertService.getConcertPopVenuesPopBandsLargeData(concertQuery);
            })
```

will represent an external call over REST norme using ``axios`` for example. Remarque that we moved all filtring, sorting and mapping actions to the nodejs services to minimize cpu operations in the database machines. These nodejs services can be scalled adequately. Do not forget that both nodejs and mongodb use V8 engine 😉. 

* <b> What do you identify as possible risks in the architecture that you described in the long run, and how would you mitigate those?</b>

All risks discribed in (ref: [mongo-1]) and,

* Nodejs out of memory in the concertService: increase it manualy according to the feedback.Use,

```node --max-old-space-size=YOUR_NUMBER_IN_GB```

* <b> What are the key elements of your architecture that would need to be monitored, and what would you use to monitor those?</b>

in priority order,

* Mongo-c
* concertService
* Mongo-b

for the mongo databases, there are mutlitple tools, see (ref: [mongo-4]). For nodejs services, we must have a good logging system, we have to implement metric middleware(s) and use tools as Prometheus and grafana.

Thanks for all,

MB



[mongo-1]: https://www.mongodb.com/blog/post/processing-2-billion-documents-a-day-and-30tb-a

[mongo-2]: https://mongoosejs.com/docs/populate.html#cross-db-populate

[mongo-3]: https://mongoosejs.com/docs/guide.html#virtuals

[mongo-4]: https://docs.mongodb.com/manual/administration/monitoring/
