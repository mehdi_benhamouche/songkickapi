import * as KoaRouter from 'koa-router';
import AppRouter from './../interfaces/router.interface';
import ConcertRouter from './concert/concert.router';
export default class APIRouter extends KoaRouter implements AppRouter {
    concertRouter = new ConcertRouter();
    constructor() {
        super();
    }
    buildRoutes = (): KoaRouter => {
        this.get('/api', async (ctx) => {
            ctx.body = `Hello from SongKick API 💋`;
            ctx.status = 200;
            return ctx;
        });
        this.use('/api/concert', this.concertRouter.buildRoutes().routes(), this.allowedMethods());
        return this;
    };
}
