import * as Joi from '@hapi/joi';
import * as KoaRouter from 'koa-router';
import Middles from 'apikoadevtoollibrary/koa.middle';
import AppRouter from '../../interfaces/router.interface';
import RequestMiddle from '../../middles/request.middle';
import ConcertHandler from './concert.handler';

export default class ConcertRouter extends KoaRouter implements AppRouter {
    concertHandler = new ConcertHandler();
    constructor() {
        super();
    }
    private filterSchema = Joi.object().keys({
        bandIds: Joi.array().items(Joi.number()).optional(),
        longitude: Joi.number().optional(),
        latitude: Joi.number().optional(),
        radius: Joi.number().optional(),
    });
    buildRoutes = (): KoaRouter => {
        this.post(
            '/v1/filter',
            Middles.validateJoiSchema(this.filterSchema),
            RequestMiddle.checkFilterParams(),
            (ctx) => this.concertHandler.getConcertsLarge(ctx),
        );
        this.post(
            '/v2/filter',
            Middles.validateJoiSchema(this.filterSchema),
            RequestMiddle.checkFilterParams(),
            (ctx) => this.concertHandler.getConcerts(ctx),
        );
        return this;
    };
}
