import * as Koa from 'koa';
import { Promise as BlueBirdPromise } from 'bluebird';
import { get, assign, map, filter } from 'lodash';
import VenueService from '../../services/venue.service';
import ConcertService from '../../services/concert.service';
import BandService from '../../services/band.service';
import ConcertInterface from '../../interfaces/concert.interface';
import AppLogger from 'apikoadevtoollibrary/app.logger';
import DataBaseTool from '../../tools/database.tool';
import BandInterface from '../../interfaces/band.interface';
import VenueInterface from '../../interfaces/venue.interface';
import AppError from 'apikoadevtoollibrary/app.error';

export default class ConcertHandler {
    // are public for testing purpose
    venueService = new VenueService();
    concertService = new ConcertService();
    bandService = new BandService();
    logger = new AppLogger(ConcertHandler.name);
    getConcertsLarge = async (ctx: Koa.Context): Promise<any> => {
        const bandIds = get(ctx, 'request.body.bandIds');
        const longitude = get(ctx, 'request.body.longitude');
        const latitude = get(ctx, 'request.body.latitude');
        const radius = get(ctx, 'request.body.radius');

        const bandQuery = {};
        const promises = [];
        if (bandIds) {
            assign(bandQuery, { id: { $in: bandIds } });
        }
        promises.push(this.bandService.getBands(bandQuery));
        if (radius && longitude && latitude) {
            promises.push(this.venueService.getVenues({}));
        } else {
            promises.push(Promise.resolve([]));
        }
        return BlueBirdPromise.all(promises)
            .spread((bands, venues) => {
                const concertQuery = { $and: [] };
                if (venues.length) {
                    const venueIds = filter(venues, (v: VenueInterface) => {
                        return DataBaseTool.isAround(v, latitude, longitude, radius);
                    }).map((v: VenueInterface) => {
                        return v.id;
                    });
                    concertQuery.$and.push({ venueId: { $in: venueIds } });
                }
                if (bands.length) {
                    const bandIds = map(bands, (b: BandInterface) => {
                        return b.id;
                    });
                    concertQuery.$and.push({ bandId: { $in: bandIds } });
                }
                if (concertQuery.$and === []) concertQuery.$and = undefined;
                return this.concertService.getConcertPopVenuesPopBandsLargeData(concertQuery);
            })
            .then((concerts: ConcertInterface[]) => {
                ctx.body = concerts
                    .sort((a, b) => {
                        if (a.date > b.date) return -1;
                        if (a.date <= b.date) return 1;
                    })
                    .map((c) => {
                        return {
                            band: c.band.name,
                            location: c.venue.name,
                            date: c.date,
                            latitude: c.venue.latitude,
                            longitude: c.venue.longitude,
                        };
                    });
                ctx.status = 200;
                return ctx;
            })
            .catch((err) => {
                const error = new AppError(ConcertHandler.name, 'getConcerts', this.errors.SKS_CON_01, err);
                this.logger.error(error);
                ctx.throw(error.publicError());
            });
    };

    getConcerts = async (ctx: Koa.Context): Promise<any> => {
        const bandIds = get(ctx, 'request.body.bandIds');
        const longitude = get(ctx, 'request.body.longitude');
        const latitude = get(ctx, 'request.body.latitude');
        const radius = get(ctx, 'request.body.radius');

        const bandQuery = {};
        if (bandIds) {
            assign(bandQuery, { bandId: { $in: bandIds } });
        }
        function handleWhere(c: ConcertInterface): boolean {
            if (longitude && latitude && radius) {
                return DataBaseTool.isAround(c.venue, latitude, longitude, radius);
            }
            return true;
        }
        try {
            const concerts = await this.concertService.getConcertPopVenuesPopBandsSmallData(bandQuery, handleWhere);
            ctx.body = concerts;
            ctx.status = 200;
            return ctx;
        } catch (err) {
            const error = new AppError(ConcertHandler.name, 'getConcerts', this.errors.SKS_CON_01, err);
            this.logger.error(error);
            ctx.throw(error.publicError());
        }
    };

    private errors = {
        SKS_CON_01: {
            code: 'SKS_CON_01',
            status: 500,
            message: 'An error has occured',
        },
    };
}
