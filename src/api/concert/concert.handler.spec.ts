import * as sinon from 'sinon';
import { expect } from 'chai';
import * as Koa from 'koa';
import ConcertHandler from './concert.handler';
import BandInterface from '../../interfaces/band.interface';
import VenueInterface from '../../interfaces/venue.interface';
import ConcertInterface from '../../interfaces/concert.interface';
import AppLogger from 'apikoadevtoollibrary/app.logger';

describe('Test concert.handler', () => {
    //TODO: test more sénaros
    const concertHandler = new ConcertHandler();
    let resultBand1: BandInterface;
    let resultBand2: BandInterface;
    let resultBand3: BandInterface;
    let resultVenue1: VenueInterface;
    let resultVenue2: VenueInterface;
    let resultVenue3: VenueInterface;
    let resultConcert1: ConcertInterface;
    let resultConcert2: ConcertInterface;
    let resultConcert3: ConcertInterface;
    let resultConcert4: ConcertInterface;
    let resultConcert5: ConcertInterface;
    let bandServiceStub: sinon.SinonStub<[any], Promise<BandInterface[]>>;
    let venueServiceStub: sinon.SinonStub<[any], Promise<VenueInterface[]>>;
    let concertServiceStub: sinon.SinonStub<[any], Promise<ConcertInterface[]>>;
    let spyFakeThrow: sinon.SinonSpy<[any], void>;
    const logger = new AppLogger('Test concert.handler');
    function fakeThrow(x: any): any {
        logger.info(x);
        return x;
    }

    describe('Test-1', () => {
        resultBand1 = {
            id: 1,
            name: 'zozo',
        };
        resultBand2 = {
            id: 2,
            name: 'zozo2',
        };
        resultBand3 = {
            id: 3,
            name: 'zozo3',
        };
        resultVenue1 = {
            id: 1,
            name: 'zozo venue',
            longitude: 56.23,
            latitude: 23.2,
        };
        resultVenue2 = {
            id: 2,
            name: 'zozo2 venue',
            longitude: 56.23,
            latitude: 23.2,
        };
        resultVenue3 = {
            id: 3,
            name: 'zozo3 venue',
            longitude: 56.23,
            latitude: 23.2,
        };

        resultConcert1 = {
            bandId: 1,
            band: {
                id: 1,
                name: 'zozo 1',
            },
            venue: {
                id: 1,
                name: 'zozo venue',
                longitude: 56.23,
                latitude: 23.2,
            },
            venueId: 2,
            date: 123567771,
        };
        resultConcert2 = {
            bandId: 2,
            venueId: 2,
            band: {
                id: 2,
                name: 'zozo 2',
            },
            venue: {
                id: 1,
                name: 'zozo venue',
                longitude: 56.23,
                latitude: 23.2,
            },
            date: 123567772,
        };
        resultConcert3 = {
            bandId: 3,
            venueId: 2,
            band: {
                id: 3,
                name: 'zozo 3',
            },
            venue: {
                id: 1,
                name: 'zozo venue',
                longitude: 56.23,
                latitude: 23.2,
            },
            date: 123567773,
        };
        resultConcert4 = {
            bandId: 3,
            venueId: 2,
            band: {
                id: 3,
                name: 'zozo 3',
            },
            venue: {
                id: 1,
                name: 'zozo venue',
                longitude: 56.23,
                latitude: 23.2,
            },
            date: 123567774,
        };
        resultConcert5 = {
            bandId: 3,
            band: {
                id: 3,
                name: 'zozo 3',
            },
            venue: {
                id: 1,
                name: 'zozo venue',
                longitude: 56.23,
                latitude: 23.2,
            },
            venueId: 2,
            date: 123567775,
        };
        const ctx1 = {
            request: {
                body: {
                    bandIds: [1, 2, 3],
                },
            },
            throw: (x: any) => spyFakeThrow(x),
        } as Koa.Context;
        const sandbox = sinon.createSandbox();
        beforeEach('Test-1', () => {
            const resband = [resultBand1, resultBand2, resultBand3];
            const resvenue = [resultVenue1, resultVenue2, resultVenue3];
            const resconcert = [resultConcert1, resultConcert2, resultConcert3, resultConcert4, resultConcert5];
            bandServiceStub = sandbox.stub(concertHandler.bandService, 'getBands').returns(Promise.resolve(resband));
            venueServiceStub = sandbox
                .stub(concertHandler.venueService, 'getVenues')
                .returns(Promise.resolve(resvenue));
            concertServiceStub = sandbox
                .stub(concertHandler.concertService, 'getConcertPopVenuesPopBandsLargeData')
                .returns(Promise.resolve(resconcert));
            spyFakeThrow = sandbox.spy(fakeThrow);
        });
        afterEach('Test-1', () => {
            bandServiceStub.restore();
            venueServiceStub.restore();
            concertServiceStub.restore();
        });

        it('Should ctx have status 200', async () => {
            const result = await concertHandler.getConcertsLarge(ctx1);
            expect(result['status']).to.be.eql(200);
        });
        it('Should ctx.body have the right elements', async () => {
            const result = await concertHandler.getConcertsLarge(ctx1);
            expect(result['body']).to.deep.equal([
                {
                    band: 'zozo 3',
                    location: 'zozo venue',
                    latitude: 23.2,
                    longitude: 56.23,
                    date: 123567775,
                },
                {
                    band: 'zozo 3',
                    location: 'zozo venue',
                    latitude: 23.2,
                    longitude: 56.23,
                    date: 123567774,
                },
                {
                    band: 'zozo 3',
                    location: 'zozo venue',
                    latitude: 23.2,
                    longitude: 56.23,
                    date: 123567773,
                },
                {
                    band: 'zozo 2',
                    location: 'zozo venue',
                    latitude: 23.2,
                    longitude: 56.23,
                    date: 123567772,
                },
                {
                    band: 'zozo 1',
                    location: 'zozo venue',
                    latitude: 23.2,
                    longitude: 56.23,
                    date: 123567771,
                },
            ]);
        });
        it('Should ctx.body not have this order of elements', async () => {
            const result = await concertHandler.getConcertsLarge(ctx1);
            expect(result['body']).to.not.have.ordered.members([
                {
                    band: 'zozo 2',
                    location: 'zozo venue',
                    latitude: 23.2,
                    longitude: 56.23,
                    date: 123567774,
                },
                {
                    band: 'zozo 3',
                    location: 'zozo venue',
                    latitude: 23.2,
                    longitude: 56.23,
                    date: 123567775,
                },
                {
                    band: 'zozo 3',
                    location: 'zozo venue',
                    latitude: 23.2,
                    longitude: 56.23,
                    date: 123567773,
                },
                {
                    band: 'zozo 2',
                    location: 'zozo venue',
                    latitude: 23.2,
                    longitude: 56.23,
                    date: 123567772,
                },
                {
                    band: 'zozo 1',
                    location: 'zozo venue',
                    latitude: 23.2,
                    longitude: 56.23,
                    date: 123567771,
                },
            ]);
        });
    });
});
