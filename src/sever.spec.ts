/* eslint-disable no-undef */
import { spy } from 'sinon';
import * as chai from 'chai';
import SongKickServer from './server';
const expect = chai.expect;

describe('Test server', () => {
    const server = new SongKickServer(1234);
    const spyUse = spy(server, 'use');
    const spyGo = spy(server, 'go');
    describe('Test methods', () => {
        it('Should use method be called 4 times in useMiddles', () => {
            server.useMiddles();
            expect(spyUse.callCount).to.be.equal(4);
        });
        it('Should go, useMiddles and connectDB methods be called in core', () => {
            server.core();
            expect(spyGo.callCount).to.be.equal(1);
            server.end();
        });
    });
});
