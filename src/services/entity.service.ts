import * as Mongoose from 'mongoose';

export default abstract class EntityService {
    model: Mongoose.Model<any>;
    constructor(model: Mongoose.Model<any>) {
        this.model = model;
    }
    protected find = async <T>(query: any): Promise<T[]> => {
        const model = this.model;
        return model.find(query);
    };

    protected aggregate = async (agg: any): Promise<any> => {
        const model = this.model;
        return model.aggregate(agg).exec();
    };
}
