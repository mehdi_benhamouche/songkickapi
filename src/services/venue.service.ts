import EntityService from './entity.service';
import { isEmpty } from 'lodash';
import VenueModel from '../models/venue.model';
import VenueInterface from '../interfaces/venue.interface';

export default class VenueService extends EntityService {
    constructor() {
        super(VenueModel.getModel());
    }
    public getVenues = async (query: any): Promise<VenueInterface[]> => {
        return this.find(query);
    };
    public getVenuesAggregation = async (agg: any): Promise<VenueInterface[]> => {
        if (isEmpty(agg)) return [];
        return this.aggregate(agg);
    };
}
