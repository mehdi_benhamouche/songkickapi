import EntityService from './entity.service';
import BandModel from '../models/band.model';
import { isEmpty } from 'lodash';
import BandInterface from '../interfaces/band.interface';

export default class BandService extends EntityService {
    constructor() {
        super(BandModel.getModel());
    }
    public getBands = async (query: any): Promise<BandInterface[]> => {
        if (isEmpty(query)) return [];
        return this.find(query);
    };
}
