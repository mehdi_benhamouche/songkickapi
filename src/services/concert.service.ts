import EntityService from './entity.service';
import { isEmpty } from 'lodash';
import ConcertInterface from '../interfaces/concert.interface';
import ConcertModel from '../models/concert.model';
import DataBaseTool from '../tools/database.tool';

export default class ConcertService extends EntityService {
    constructor() {
        super(ConcertModel.getModel());
    }
    public getConcerts = async (query: any): Promise<ConcertInterface[]> => {
        if (isEmpty(query)) return [];
        return this.find(query);
    };

    public getConcertPopVenuesPopBandsLargeData = async (query: any): Promise<ConcertInterface[]> => {
        return ConcertModel.getModel().find(query).populate('venue').populate({ path: 'band', select: 'name' }).exec();
    };
    // TODO: create an interface for the resulting promise
    public getConcertPopVenuesPopBandsSmallData = async (query: any, fn: Function): Promise<any[]> => {
        return ConcertModel.getModel()
            .find(query)
            .populate('venue')
            .populate({ path: 'band', select: 'name' })
            .map((ac: ConcertInterface[]) => {
                return ac
                    .filter((c) => fn(c))
                    .map((c) => {
                        return {
                            band: c.band.name,
                            location: c.venue.name,
                            date: c.date,
                            latitude: c.venue.latitude,
                            longitude: c.venue.longitude,
                        };
                    });
            })
            .sort({ date: -1 })
            .exec();
    };
}
