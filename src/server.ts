import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import * as koaJsonError from 'koa-json-error';
import * as Mongoose from 'mongoose';
import { omit } from 'lodash';
import Logger from 'apikoadevtoollibrary/app.logger';
import AppError from 'apikoadevtoollibrary/app.error';
import APIRouter from './api/api.router';

export default class SongKickServer extends Koa {
    private port: number;
    private listenAction = null;
    private logger = new Logger(SongKickServer.name);
    private apiRouter = new APIRouter();
    constructor(port: number) {
        super();
        this.port = port;
    }
    useMiddles() {
        this.use(bodyParser());
        this.use(
            koaJsonError({
                /* Remove stack trace from http errors */
                postFormat: (_err, obj) => omit(obj, 'stack'),
            }),
        );
        this.use(this.apiRouter.buildRoutes().routes());
        this.use(this.apiRouter.allowedMethods());
    }
    go = (): void => {
        this.listenAction = this.listen(this.port, () => {
            this.logger.info(`${SongKickServer.name} Server is running at port ${this.port} 😎 🚀`);
        }).on('error', (err) => {
            const error = new AppError(SongKickServer.name, this.go.name, this.errors['SKS01'], err);
            this.logger.error(error);
            this.end();
        });
    };
    connectDB = (): void => {
        Mongoose.connect('mongodb://mongodb:27017/SONGKICK_DATABASE', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
    };
    end = (): void => {
        if (this.listenAction) {
            this.listenAction.close(() => {
                this.logger.warn('Server closed!');
            });
        }
    };
    core = (): void => {
        this.useMiddles();
        this.go();
    };
    private errors = {
        SKS01: {
            code: 'SKS01',
            status: 500,
            message: 'server error',
        },
    };
}
