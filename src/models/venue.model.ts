import * as Mongoose from 'mongoose';
export default class VenueModel {
    private static model = new Mongoose.Schema({
        id: { type: Number, required: true, index: true },
        name: { type: String, required: true },
        latitude: { type: Number, required: true },
        longitude: { type: Number, required: true },
    });
    public static getModel(): Mongoose.Model<any> {
        return Mongoose.model('venue', VenueModel.model, 'venues');
    }
}
