import * as Mongoose from 'mongoose';
export default class ConcertModel {
    private static model = new Mongoose.Schema({
        bandId: { type: Number, required: true },
        venueId: { type: Number, required: true },
        date: { type: Number, required: true },
    });
    public static getModel(): Mongoose.Model<any> {
        this.model.virtual('venue', {
            ref: 'venue',
            localField: 'venueId',
            foreignField: 'id',
            justOne: true,
        });
        this.model.virtual('band', {
            ref: 'band',
            localField: 'bandId',
            foreignField: 'id',
            justOne: true,
        });
        return Mongoose.model('concert', ConcertModel.model, 'concerts');
    }
}
