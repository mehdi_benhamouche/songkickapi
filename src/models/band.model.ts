import * as Mongoose from 'mongoose';
export default class BandModel {
    private static model = new Mongoose.Schema({
        id: { type: Number, required: true, index: true },
        name: { type: String, required: true },
    });
    public static getModel(): Mongoose.Model<any> {
        return Mongoose.model('band', BandModel.model, 'bands');
    }
}
