import VenueInterface from '../interfaces/venue.interface';

export default class DataBaseTool {
    /**
     * WIP
     * returns the aggregation pipline
     * @param lat latitude
     * @param long longitude
     * @param rad radius
     */
    static getVenuesPipline(lat: number, long: number, rad: number): any {
        return [
            {
                $match: {
                    $lt: [
                        {
                            $acos: {
                                $add: [
                                    {
                                        $multiply: [
                                            {
                                                $sin: {
                                                    $degreesToRadians: 'latitude',
                                                },
                                            },
                                            {
                                                $sin: {
                                                    $degreesToRadians: lat,
                                                },
                                            },
                                        ],
                                    },
                                    {
                                        $multiply: [
                                            {
                                                $cos: {
                                                    $degreesToRadians: 'latitude',
                                                },
                                            },
                                            {
                                                $cos: {
                                                    $degreesToRadians: lat,
                                                },
                                            },
                                            {
                                                $cos: {
                                                    $degreesToRadians: {
                                                        $subtract: [long, 'longitude'],
                                                    },
                                                },
                                            },
                                        ],
                                    },
                                ],
                            },
                        },
                        rad / 6371,
                    ],
                },
            },
        ];
    }
    /**
     * return true when the point (v.latitude, v.longitude) is in the cercle formed by
     * the point (lat, long) with radius rad
     * @param v venue object
     * @param lat latitude
     * @param long longitude
     * @param rad radius
     * @return boolean
     */
    static isAround(v: VenueInterface, lat: number, long: number, rad: number): boolean {
        // check this formula again
        return (
            Math.acos(
                Math.sin((v.latitude * Math.PI) / 180) * Math.sin((lat * Math.PI) / 180) +
                    Math.cos((lat * Math.PI) / 180) *
                        Math.cos((v.latitude * Math.PI) / 180) *
                        Math.cos(((v.longitude - long) * Math.PI) / 180),
            ) <=
            rad / 6371
        );
    }
}
