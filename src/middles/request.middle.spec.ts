/* eslint-disable no-undef */

import * as Koa from 'koa';
import * as sinon from 'sinon';
import RequestMiddle from './request.middle';
import AppLogger from 'apikoadevtoollibrary/app.logger';
import { expect } from 'chai';
import { assign } from 'lodash';

describe('Test request.middle', () => {
    const logger = new AppLogger('Test request.middle');
    function fakeThrow(x: any) {
        logger.info(x);
        return x;
    }
    function fakeNext() {
        logger.info('Next called');
    }
    const middleValidate = RequestMiddle.checkFilterParams();
    let spyFakeNext: sinon.SinonSpy<[], void>;
    let spyFakeThrow: sinon.SinonSpy<[any], void>;
    it('Should checkFilterParams be well defined', () => {
        expect(RequestMiddle.checkFilterParams).to.not.be.an('undefined');
    });

    it('Should middleValidate be a function', () => {
        expect(middleValidate).to.be.a('function');
    });

    it('Should checkFilterParams middle-name be validate', () => {
        expect(middleValidate.name).to.be.equal('validate');
    });

    describe('checkFilterParams ctx1 KO', () => {
        const sandbox = sinon.createSandbox();
        before('ctx1', () => {
            spyFakeNext = sandbox.spy(fakeNext);
            spyFakeThrow = sandbox.spy(fakeThrow);
            const ctx1 = {
                request: {
                    body: {
                        property: 'zozo',
                    },
                },
                throw: (x: any) => spyFakeThrow(x),
            } as Koa.Context;
            middleValidate(ctx1, spyFakeNext);
        });
        after('ctx1', () => {
            sandbox.restore();
        });
        it('Should checkFilterParams next be called', () => {
            expect(spyFakeNext).to.be.calledOnce();
        });

        it('Should checkFilterParams fakeThrow be called', () => {
            expect(spyFakeThrow).to.be.called();
        });
    });
    describe('checkFilterParams ctx2 OK', () => {
        const sandbox = sinon.createSandbox();
        before('ctx2', () => {
            spyFakeNext = sandbox.spy(fakeNext);
            spyFakeThrow = sandbox.spy(fakeThrow);
            const ctx2 = {
                request: {
                    body: {
                        bandIds: [1, 2, 3],
                    },
                },
                throw: (x: any) => spyFakeThrow(x),
            } as Koa.Context;
            middleValidate(ctx2, spyFakeNext);
        });
        after('ctx2', () => {
            sandbox.restore();
        });
        it('Should checkFilterParams next be called', () => {
            expect(spyFakeNext).to.be.calledOnce();
        });

        it('Should checkFilterParams fakeThrow not be called', () => {
            expect(spyFakeThrow).to.be.not.called();
        });
    });
    describe('checkFilterParams ctx3 OK', () => {
        const sandbox = sinon.createSandbox();
        before('ctx3', () => {
            spyFakeNext = sandbox.spy(fakeNext);
            spyFakeThrow = sandbox.spy(fakeThrow);
            const ctx3 = {
                request: {
                    body: {
                        longitude: 23.9098,
                        latitude: 23.098,
                        radius: 56,
                    },
                },
                throw: (x: any) => spyFakeThrow(x),
            } as Koa.Context;
            middleValidate(ctx3, spyFakeNext);
        });
        after('ctx3', () => {
            sandbox.restore();
        });
        it('Should checkFilterParams next be called', () => {
            expect(spyFakeNext).to.be.calledOnce();
        });

        it('Should checkFilterParams fakeThrow not be called', () => {
            expect(spyFakeThrow).to.be.not.called();
        });
    });
    describe('checkFilterParams ctx4 KO', () => {
        const sandbox = sinon.createSandbox();
        before('ctx4', () => {
            spyFakeNext = sandbox.spy(fakeNext);
            spyFakeThrow = sandbox.spy(fakeThrow);
            const ctx4 = {
                request: {
                    body: {
                        longitude: 23.9098,
                        radius: 56,
                    },
                },
                throw: (x: any) => spyFakeThrow(x),
            } as Koa.Context;
            middleValidate(ctx4, spyFakeNext);
        });
        after('ctx4', () => {
            sandbox.restore();
        });
        it('Should checkFilterParams next be called', () => {
            expect(spyFakeNext).to.be.calledOnce();
        });

        it('Should checkFilterParams fakeThrow be called', () => {
            expect(spyFakeThrow).to.be.called();
        });
    });
    describe('checkFilterParams ctx5 OK', () => {
        const sandbox = sinon.createSandbox();
        before('ctx5', () => {
            spyFakeNext = sandbox.spy(fakeNext);
            spyFakeThrow = sandbox.spy(fakeThrow);
            const ctx5 = {
                request: {
                    body: {
                        bandIds: [1, 2, 3],
                        longitude: 23.9098,
                        radius: 56,
                    },
                },
                throw: (x: any) => spyFakeThrow(x),
            } as Koa.Context;
            middleValidate(ctx5, spyFakeNext);
        });
        after('ctx5', () => {
            sandbox.restore();
        });
        it('Should checkFilterParams next be called', () => {
            expect(spyFakeNext).to.be.calledOnce();
        });

        it('Should checkFilterParams fakeThrow be not called', () => {
            expect(spyFakeThrow).to.be.not.called();
        });
    });
    describe('checkFilterParams ctx6 KO', () => {
        const sandbox = sinon.createSandbox();
        let ctx6: any;
        const error = new Error();
        assign(error, {
            name: 'SKS02',
            status: 400,
            message: 'Bad request, somme parameters are required',
        });
        error.stack = undefined;
        before('ctx6', () => {
            spyFakeNext = sandbox.spy(fakeNext);
            spyFakeThrow = sandbox.spy(fakeThrow);
            ctx6 = {
                request: {
                    body: {
                        bandIds: [],
                        latitude: 23.9098,
                        radius: 56,
                    },
                },
                throw: (x: any) => spyFakeThrow(x),
            } as Koa.Context;
            middleValidate(ctx6, spyFakeNext);
        });
        after('ctx6', () => {
            sandbox.restore();
        });
        it('Should checkFilterParams next be called', () => {
            expect(spyFakeNext).to.be.calledOnce();
        });
        it('Should checkFilterParams fakeThrow be called', () => {
            expect(spyFakeThrow).to.be.called();
        });
    });
});
