import * as Koa from 'koa';
import { get } from 'lodash';
import AppError from 'apikoadevtoollibrary/app.error';
import AppLogger from 'apikoadevtoollibrary/app.logger';
export default class RequestMiddle {
    static checkFilterParams = () => {
        const validate = async (ctx: Koa.Context, next: any) => {
            const bandIds = get(ctx, 'request.body.bandIds');
            const longitude = get(ctx, 'request.body.longitude');
            const latitude = get(ctx, 'request.body.latitude');
            const radius = get(ctx, 'request.body.radius');
            if (!bandIds || bandIds.length === 0) {
                if (!longitude || !latitude || !radius) {
                    const error = new AppError(
                        RequestMiddle.name,
                        RequestMiddle.checkFilterParams.name,
                        RequestMiddle.errors.SKS02,
                    );
                    new AppLogger(RequestMiddle.name).error(error);
                    ctx.throw(error.publicError());
                }
            }
            await next();
        };
        return validate;
    };
    private static errors = {
        SKS02: {
            code: 'SKS02',
            status: 400,
            message: 'Bad request, somme parameters are required',
        },
    };
}
