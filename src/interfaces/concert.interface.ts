import BandInterface from './band.interface';
import VenueInterface from './venue.interface';

export default interface ConcertInterface {
    bandId: number;
    venueId: number;
    band: BandInterface;
    venue: VenueInterface;
    date: number;
}
