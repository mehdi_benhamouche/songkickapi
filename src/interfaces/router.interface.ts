import * as KoaRouter from 'koa-router';
export default interface AppRouter {
    buildRoutes(): KoaRouter;
}
