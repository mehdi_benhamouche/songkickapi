export default interface VenueInterface {
    id: number;
    name: string;
    longitude: number;
    latitude: number;
}
