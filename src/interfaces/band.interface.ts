export default interface BandInterface {
    id: number;
    name: string;
}
