mongo localhost/SONGKICK_DATABASE &&
mongoimport --db=SONGKICK_DATABASE --collection=concerts  --jsonArray --file=concerts.json &&
mongoimport --db=SONGKICK_DATABASE --collection=bands  --jsonArray --file=bands.json &&
mongoimport --db=SONGKICK_DATABASE --collection=venues  --jsonArray --file=venues.json