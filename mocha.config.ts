import * as chai from 'chai';
import dirtyChai = require('dirty-chai');
import * as sinon from 'sinon';
import * as sinonChai from 'sinon-chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as chaiThings from 'chai-things';
import chaiEqualInAnyOrder = require('deep-equal-in-any-order');
import chaiHttp = require('chai-http');
import { assign } from 'lodash';

// provides a set of custom assertions for using the Sinon.JS spy, stub, and mocking framework
// with the Chai assertion
chai.use(sinonChai);

// Chai as Promised extends Chai with a fluent language for asserting facts about promises.
chai.use(chaiAsPromised);
chai.use(chaiHttp);

// Chai Things adds support to Chai for assertions on array elements.
chai.use(chaiThings);

// Function form for terminating assertion properties.
chai.use(dirtyChai);

// Extend Chai Assertion library with tests for array deep equal
chai.use(chaiEqualInAnyOrder);

// add globals
assign(global, { expect: chai.expect });
assign(global, { chai: chai });
assign(global, { sinon: sinon.createSandbox() });
