@startuml app_class_digram

class Server {
    -- extends koa server --
    - number port
    - any listenAction
    - Logger loger
    - array errors
    + APIRouter apiRouter
    + useMiddles()
    + go()
    + connectDB()
    + end()
    + core()
}

class APIRouter {
    -- extends koa router --
    - ConcertRouter concertRouter
    + buildRoutes()
}

class ConcertRouter {
    -- extends koa router --
    -- implements RouterInterface --
    - ConcertHandler concertHandler
    + buildRoutes()
}

class ConcertHandler {
    + VenueService venueService
    + ConcertService concertService
    + BandService bandService
    + AppLogger logger
    - Errors
    + public getCOncerts()
}

class RequestMiddle {
    -- static --
    - errors
    + public checkFilterParams()
}

class VenueService {
    + getVenues()
    + getVenuesAggregation()
}

class BandService {
    + getBands()
}

class ConcertService {
    + getConcerts()
}

class EntityService {
    - model
    # find()
    # aggregate()
}

class BandModel {
    -- static --
    - model
    + getModel()
}

class VenueModel {
    -- static --
    - model
    + getModel()
}

class ConcertModel {
    -- static --
    - model
    + getModel()
}

class DatabaseTool {
    -- static --
    + getVenuesPipline()
    + isAround()
}


Server --* APIRouter
APIRouter --* ConcertRouter
ConcertRouter .. RequestMiddle
ConcertRouter --* ConcertHandler
ConcertHandler --* ConcertService
ConcertHandler --* VenueService
ConcertHandler --* BandService
ConcertHandler .. DatabaseTool
BandService .. BandModel
BandService --|> EntityService
VenueService .. VenueModel
VenueService --|> EntityService
ConcertService .. ConcertModel
ConcertService --|> EntityService

@enduml